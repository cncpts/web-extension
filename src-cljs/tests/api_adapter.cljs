(ns tests.api-adapter
  (:require [cljs.test :refer (deftest async use-fixtures is)])
  (:require [api-adapter.core :refer (health-check init query-log log-activity)]))

(defn log [res] (js/console.log res))

(use-fixtures :once
  {:before init})


(deftest healthz-test
  (async done
         (health-check
          (fn [res]
            (is (= (res :status) 200))
            (done)))))

(deftest query-test
  (async done
         (query-log
          (fn [data]
            (is (object? (.-data data)))
            (done)))))

(def json-to-insert 
  (clj->js [{:activity "fire-fox"
             :start_time (js/Date. (.now js/Date))
             :duration "20"
             :device "laptop"
             :payload {:url "https://www.N1NJ4.com"}}
            {:activity "web-extension-dev"
             :start_time (js/Date. (+ (.now js/Date) 20000))
             :duration "300"
             :device "laptop"
             :payload {:url "https://www.T1M0N.com"}}]))

(deftest log-activity-test
  (async done
         (log-activity json-to-insert
          (fn [data]
            (is (object? (.-data data)))
            (done)))))

