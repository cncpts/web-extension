(ns api-adapter.core
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [cljs-http.client :as http]
            [cljs.core.async :refer [<!]])
  (:require [re-graph.core :as rg]))

(goog-define HASURA_ENDPOINT "")
(goog-define HASURA_ADMIN_SECRET "")

(defn health-check [cb]
  (go (let [response (<! (http/get (str HASURA_ENDPOINT "/healthz")))]
        (cb response))))

(defn ^:export init []
  (rg/init {:http-url (str HASURA_ENDPOINT "/v1/graphql")
            :ws-url nil
            :http-parameters {:with-credentials? false
                              :headers {"X-Hasura-Admin-Secret" HASURA_ADMIN_SECRET}}}))

(defn rg-data->js-cb [cb] (fn [payload] (-> payload (clj->js) (cb))))

(defn ^:export query-log [cb]
  (rg/query
   "{
      logs_activities(limit: 10) {
        id
        activity
        start_time
        duration
        device
        payload
      }
    }"
   {}
   (rg-data->js-cb cb)))


(defn ^:export log-activity [data cb]
  (rg/mutate
"mutation logActivity($entries: [logs_activities_insert_input!]!) {
  insert_logs_activities(objects: $entries) {
    affected_rows
  }
}"
   {:entries (js->clj data)}
   (rg-data->js-cb cb)))