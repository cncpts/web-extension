/* global cross devices Storage */
const Base = require("lowdb/adapters/Base");

export class LocalStorage extends Base {
  read() {
    return browser.storage.local.get(this.source).then(data => {
      console.debug("READ: ", data);
      if (data[this.source]) {
        return this.deserialize(data[this.source]);
      } else {
        browser.storage.local.set({
          [this.source]: this.serialize(this.defaultValue)
        });
        return this.defaultValue;
      }
    });
  }

  write(data) {
    console.debug("WRITE: ", data);
    browser.storage.local.set({ [this.source]: this.serialize(data) });
  }
}

export class SyncStorage extends Base {
  read() {
    return browser.storage.sync.get(this.source).then(data => {
      console.debug("READ: ", data);
      if (data[this.source]) {
        return this.deserialize(data[this.source]);
      } else {
        browser.storage.sync.set({
          [this.source]: this.serialize(this.defaultValue)
        });
        return this.defaultValue;
      }
    });
  }

  write(data) {
    console.debug("WRITE: ", data);
    browser.storage.sync.set({ [this.source]: this.serialize(data) });
  }
}
