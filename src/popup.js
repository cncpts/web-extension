import React from "react";
import ReactDOM from "react-dom";
import { ButtonGroup, Button } from "reactstrap";
import * as R from "ramda";
import low from "lowdb/lib/fp";
import { CSVLink } from "react-csv";
import { LocalStorage } from "./WebExtension";

class Popup extends React.Component {
  constructor(props) {
    super(props);
    this.state = { records: null };
  }

  componentDidMount() {
    low(new LocalStorage("conceptual")).then(db => {
      this.setState({ records: db("records") });
    });
  }

  render() {
    const { records } = this.state;
    return records ? <Options records={records(R.clone)} /> : "Loading";
  }
}

const Options = ({ records }) => (
  <ButtonGroup vertical>
    <Button className="bg-light text-dark"><CSVLink data={records}>Export CSV</CSVLink></Button>
  </ButtonGroup>
);

ReactDOM.render(<Popup />, document.getElementById("popup"));
