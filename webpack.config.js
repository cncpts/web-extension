const path = require("path");

module.exports = {
  entry: {
    // Each entry in here would declare a file that needs to be transpiled
    // and included in the extension source.
    // For example, you could add a background script like:
    // background: './src/background.js',
    popup: "./src/popup.js",
    background: "./src/background.js"
  },
  output: {
    // This copies each source entry into the extension dist folder named
    // after its entry config key.
    path: path.join(path.resolve(__dirname), "public", "js"),
    filename: "[name].js"
  },
  module: {
    // This transpiles all code (except for third party modules) using Babel.
    rules: [
      {
        test: /\.m?js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"]
          }
        }
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"]
      }
    ]
  },
  resolve: {
    // This allows you to import modules just like you would in a NodeJS app.
    extensions: [".js", ".jsx", ".css"],
    modules: ["src", "node_modules"]
  },
  plugins: [],
  // This will expose source map files so that errors will point to your
  // original source files instead of the transpiled files.
  devtool: "sourcemap",
  mode: "development"
};
