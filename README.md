# Firefox [Web-Extention][web-ext] With [React][react] and [ClojureScript][cljs]

# Quick start

### 1. Install

    yarn install

### 2. Build ClojureScript modules

    . .env
    yarn cljs-dev

### 3. Build React modules

    yarn build

### 4. Run extention in browser

    yarn start

# ClojureScript

The graphql-api module is written in cljs.

### Build & Tests

    . .env
    yarn cljs-dev

Compiled files will be in `target/build/`

Open <http://localhost:8021> in the browser to see tests report.

### Release

The `release` configuration uses advance optimizations that greatly reduces the js files size.

    . .env
    yarn cljs-release

# React

### Build

The `build` option uses watch to hot-reload code for easy development.

    yarn build

### Release

The `release` configuration uses advance optimizations that greatly reduces the js files size.

    yarn release

# Web-Ext

## Run extension in browser

    yarn start

## Run extension in android

#### 1. Checkout branch `production/mobile` & merge the changes from master

#### 2. Make sure to [enable developer options and debugging](https://developer.android.com/studio/debug/dev-options.html#enable)

#### 3. Connect your device to the computer with USB

#### 4. Find your device id

    adb devices

#### 5. Start your extension on firefox for android

    yarn start-android --android-device ${YOUR_DEVICE_ID}

> **Note:** *Don't forget to edit .env file and rebuild cljs src, the mobile device probably doesn't have a server running on `http://localhost:8080`.*

### Release

#### 1. Checkout one of the production/* branches

#### 2. Update version in `public/manifest.json`

#### 3. build the extension with the right variables

    . .env.prod
    yarn cljs-release
    yarn release
    yarn sign

# Firefox Test Docker Image

Docker image containing firefox and nodejs that can run tests against browser UI.

### Build

    yarn build-firefox-image

### Push

    yarn push-firefox-image

#### Icons

The icon for this extension is provided by [icons8](https://icons8.com/).

[react]: https://facebook.github.io/react/
[cljs]: https://clojurescript.org/
[web-ext]: https://developer.mozilla.org/en-US/Add-ons/WebExtensions/Getting_started_with_web-ext
